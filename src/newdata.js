import React, {Component} from 'react';
import {connect} from 'react-redux';


class NewData extends Component {
    constructor() {
        super();
        this.titleChange = this.titleChange.bind(this);
    }
    titleChange = (e) => {
        // console.log(e.target.value);
        this.props.dispatch({type:'TITLE',payload: [{title: e.target.value}]});
    };

    render() {
        // console.log(this.props);
        return (
        <div style={{margin: 20, textAlign: "center",align: "center",border: "1px solid #000000"}}>
            <p>New Count Data: {this.props.number.count}</p>
            <input type="text" placeholder={this.props.article.title} name="title" onChange={this.titleChange}/>
        </div>
        );
    }
}

const mapStateToProps = (state) => (
    {
        number:{
            count: state.number.count,
            by: state.number.by
        },
        article: {
            title: state.article.title
        }
    }
);

export default connect(mapStateToProps) (NewData);
