import {combineReducers, createStore} from 'redux';
// Number Reducer
import TheNumber from '../reducers/number';
// Title, Body Reducer
import Article from '../reducers/titlechange';

// combining two reducers
const combined = combineReducers({number:TheNumber,article:Article});
const store = createStore(combined);
export default store;
