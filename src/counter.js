import React, {Component} from 'react';
import {connect} from 'react-redux';
// import mapStateToProps from "react-redux/es/connect/mapStateToProps";


class Counter extends Component {
    increment = (amount) => {
        this.props.dispatch({type: 'INCREMENT', payload:[{by:amount}]});
    };

    decrement = (amount) => {
        this.props.dispatch({type: 'DECREMENT', payload:[{by:amount}]});
    };
    render() {
        // console.log(this.props);
        return (
          <div style={{textAlign: "center",align: "center"}}>
            <h1>{this.props.article.title}</h1>
            <p>{this.props.article.body}</p>
            <button onClick={()=> {this.decrement(2)}}>Decrement</button>
            <p>Count: {this.props.number.count}</p>
            <button onClick={()=> {this.increment(2)}}>Increment</button>
          </div>
        );
    }
}

const mapStateToProps = (state) => (
    {
        number:{
            count: state.number.count,
            by: state.number.by
        },
        article: {
            title: state.article.title,
            body: state.article.body
        }
    }
);

export default connect(mapStateToProps) (Counter);
