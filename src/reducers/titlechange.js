const initialStore = {
    title: 'I am a Counter',
    body: 'Body changed now.'
};

function theNumber(state = initialStore,action) {
    switch(action.type) {
        case 'TITLE': {
            return {
                ...state,
                title: action.payload[0].title
            };
        }
        case 'BODY': {
            return {
                ...state,
                title: action.payload[0].body
            };
        }
        default: {
            return state;
        }
    }
}

export default theNumber;
