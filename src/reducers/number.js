const initialStore = {
    count: 43,
    by: 1
};

function getRendom() {
    return parseInt(Math.random()*10);
}

function reducer(state = initialStore,action) {
    const ran = getRendom();
    switch(action.type) {
        case 'INCREMENT': {
            return {
                ...state,
                count: state.count+action.payload[0].by,
                by: ran
            };
        }
        case 'DECREMENT': {
            return {
                ...state,
                count: state.count-action.payload[0].by,
                by: ran
            };
        }
        default: {
            return state;
        }
    }
}

export default reducer;
